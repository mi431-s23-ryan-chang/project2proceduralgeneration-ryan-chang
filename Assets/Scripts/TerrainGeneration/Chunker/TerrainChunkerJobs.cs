using Unity.Jobs;
using UnityEngine;

public class TerrainChunkerJobs : TerrainChunker
{
    [Header("Autogenerated values")]
	private ShapeGeneratorJob shapeGenerator;
    private JobHandle runningShape;

    protected override bool CheckShapeDone()
    {
        return runningShape.IsCompleted;
    }

    protected override bool CheckShapeCancelled()
    {
        return shapeGenerator.Cancelled;
    }

    protected override void CreateShapeGenerator()
    {
        shapeGenerator = new(crawlOffset, gridPosition, heightResolution,
            heightScale, master.octaves);
        runningShape = shapeGenerator.Schedule();
    }

    protected override float[,] GetHeights()
    {
        runningShape.Complete();
        return shapeGenerator.Retrieve();
    }

    protected override void CleanUp()
    {
        shapeGenerator.Cancel(runningShape);
    }

    protected override float[,,] GetAlphamap()
    {
        throw new System.NotImplementedException();
    }
}