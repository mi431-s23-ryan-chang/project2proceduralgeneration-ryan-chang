﻿using System.Collections;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    private void FixedUpdate()
    {
        transform.Rotate(Vector3.back * 1f);
    }
}