using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public static class EditorExtensions
{
    /// <summary>
    /// Gets the object from the serialized property.
    /// </summary>
    /// <typeparam name="T">Type of object to retrieve.</typeparam>
    /// <param name="property">Property to retrieve from.</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentNullException"></exception>
    public static T GetObjectFromSerialized<T>(this SerializedProperty property)
    {
        // // Clean path.
        // string rawPath = property.propertyPath.Replace(".Array.data[", "[");
        // string[] path = rawPath.Split('.');

        // foreach (var sp in path)
        // {
        //     // Try to look for array markers.
            
        // }

        var targetObject = property.serializedObject.targetObject;
        var targetObjectClassType = targetObject.GetType();
        // var path = property.propertyPath;
        var datas = targetObjectClassType.GetField("datas");
        var dt = datas.GetType();
        // var thingy = datas.GetEnumerator();

        var field = targetObjectClassType.GetField(property.propertyPath);

        if (field != null)
        {
            return (T)field.GetValue(targetObject);
        }
        else
        {
            throw new System.ArgumentNullException($"Field is null, targetObjectClassType is {targetObjectClassType}.");
        }

        throw new NotImplementedException();
    }
}