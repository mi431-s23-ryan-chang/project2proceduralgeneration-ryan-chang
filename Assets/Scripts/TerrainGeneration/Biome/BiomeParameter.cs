﻿using System.Collections;
using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Parameter used to control the generation of the biome.
/// </summary>
[Serializable]
public class BiomeParameter
{
    #region Variables
    [Header("User settings")]
    [Tooltip("Name of biome parameter, like temperature.")]
	public string name;

    [Tooltip("What is used to generate the biome.")]
    public List<Octave> map;

    [Tooltip("Color to use for debugging.")]
    public Color debugColor;
    #endregion

    public void DrawDebug(float x, float y)
    {
        Vector3 start = new(x, 0, y);
        Vector3 end = start + map.Sum(o => o.Calculate(new(x, y))) * Vector3.up;
        Debug.DrawLine(start, end, debugColor, 10);
    }

    /// <summary>
    /// Reseeds the map and shapeMap octaves by randomizing their
    /// offsets.
    /// </summary>
    public void ReseedOctaves()
    {
        map = map.ReseedOctaves();
    }
}