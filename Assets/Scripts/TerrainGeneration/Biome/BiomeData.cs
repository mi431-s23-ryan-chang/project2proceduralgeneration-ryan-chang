using System;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// Contains the actual data that will be used to dictate the biome.
/// </summary>
[Serializable]
public class BiomeData
{
    #region Variables
    [Header("User settings")]
    [Tooltip("List curves that pertain to each specific biome parameter.")]
    public ParameterTargets parameterTargets;

    [Tooltip("What is used to generate the terrain.")]
    public List<Octave> shapeGenerate;

    [Tooltip("What is used to map terrain materials to heights.")]
    public List<TerrainHeightMap> terrainMap;
    #endregion

    /// <summary>
    /// Reseeds the map and shapeMap octaves by randomizing their
    /// offsets.
    /// </summary>
    public void ReseedOctaves()
    {
        shapeGenerate = shapeGenerate.ReseedOctaves();
    }

    /// <summary>
    /// Reset the parameter targets.
    /// </summary>
    /// <param name="targets"></param>
    public void ResetParamTargets(List<BiomeParameter> targets)
    {
        parameterTargets = new(targets);
    }
}

[Serializable]
public struct TerrainHeightMap
{
    public float minHeight;
    public float maxHeight;

    public Texture texture;
}