using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// A weighted, undirected graph data structure using adjacency map.
/// </summary>
/// <typeparam name="T">Any IEquatable type.</typeparam>
public class Graph<T> : IEnumerable<Vertex<T>> where T : IEquatable<T>
{
    /* Variables */
    private const string VERTEX_VISIT_ID = "GraphInternal";

    public enum EnumerationMethod
    {
        /// <summary>
        /// https://en.wikipedia.org/wiki/Breadth-first_search
        /// </summary>
        BreathFirst,
        /// <summary>
        /// https://en.wikipedia.org/wiki/Depth-first_search
        /// </summary>
        DepthFirst
    }

    /// <summary>
    /// Selects the method of enumeration
    /// </summary>
    public EnumerationMethod EnumMethod { get; private set; }

    /// <summary>
    /// The root vertex. This determines where graph enumeration will begin.
    /// </summary>
    public Vertex<T> Root { get; private set; }

    /// <summary>
    /// Dictionary of verticies, <id, vertex>.
    /// </summary>
    public Dictionary<T, Vertex<T>> Verticies { get; set; }

    /// <summary>
    /// The number of elements in this graph.
    /// </summary>
    public int Count => Verticies.Count;

    /* Constructors */
    /// <summary>
    /// Default constructor.
    /// </summary>
    public Graph()
    {
        Verticies = new Dictionary<T, Vertex<T>>();
    }
    
    /// <summary>
    /// Creates a graph.
    /// </summary>
    /// <param name="verticies">Verticies to assign to the graph.</param>
    public Graph(Dictionary<T, Vertex<T>> verticies)
    {
        this.Verticies = verticies;
        Root = verticies.Values.First();
    }

    /// <summary>
    /// Constructs a new graph from a path. See AStarSearch.
    /// </summary>
    /// <param name="path">A dictionary, where the second vertex is what preceeds the first vertex.</param>
    public Graph(Dictionary<Vertex<T>, Vertex<T>> path)
    {
        Verticies = new Dictionary<T, Vertex<T>>();
        foreach (var first in path.Keys)
        {
            Add(path[first].id, first.id);
        }
    }

    /* Methods */
    /// <summary>
    /// Creates a single vertex with no connections.
    /// </summary>
    /// <param name="newId">The new id to add.</param>
    public void Add(T newId)
    {
        Verticies[newId] = new Vertex<T>(newId);

        if (Root == null)
        {
            Root = Verticies[newId];
        }
    }

    /// <summary>
    /// Adds an edge to the graph between fromId and toId. Inserts the verticies as needed.
    /// </summary>
    /// <param name="fromId">The starting vertex.</param>
    /// <param name="toId">The ending vertex.</param>
    /// <param name="weight">Weight of edge.</param>
    public void Add(T fromId, T toId, float weight)
    {
        if (!Verticies.ContainsKey(fromId))
        {
            Add(fromId);
        }

        if (!Verticies.ContainsKey(toId))
        {
            Add(toId);
        }

        Verticies[fromId].adjacent[toId] = weight;
    }

    /// <summary>
    /// Adds an edge with a weight of 0 to the graph between fromId and toId. Inserts the verticies as needed.
    /// </summary>
    /// <param name="fromId">The starting vertex.</param>
    /// <param name="toId">The ending vertex.</param>
    public void Add(T fromId, T toId)
    {
        Add(fromId, toId, 0);
    }

    /// <summary>
    /// Resets the visited boolean for each vertex in the graph.
    /// </summary>
    /// <param name="vertexVisitID">The visit id for the vertex.</param>
    public void ResetVerticies(string vertexVisitID)
    {
        foreach (var vertex in Verticies)
        {
            vertex.Value.SetVisited(vertexVisitID, false);
        }
    }

    /// <summary>
    /// Checks if this graph has a vertex at <paramref name="coord"/>.
    /// </summary>
    /// <param name="coord">The coordinates of the supposed vertex.</param>
    /// <returns>True if there is a vertex there, else false.</returns>
    public bool HasVertex(T coord)
    {
        return Verticies.ContainsKey(coord);
    }

    /// <summary>
    /// Gets a vertex by it's coordinates.
    /// </summary>
    /// <param name="coord">Coordinates of the vertex.</param>
    /// <returns>A vertex if one exists at the coordinates specified, else null.</returns>
    public Vertex<T> VertexByID(T coord)
    {
        if (Verticies.ContainsKey(coord))
            return Verticies[coord];
        else
            return null;
    }

    /// <summary>
    /// Determines if an edge exists between fromID and toID.
    /// </summary>
    /// <param name="fromID">ID to come from.</param>
    /// <param name="toID">ID to go to.</param>
    /// <returns>True if there exists an edge between fromID and toID, else false.</returns>
    public bool HasEdge(T fromID, T toID)
    {
        if (HasVertex(fromID) && HasVertex(toID))
        {
            var fromV = Verticies[fromID];
            if (fromV.adjacent.ContainsKey(toID))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Gets the edge weight from fromID to toID.
    /// </summary>
    /// <param name="fromID">Coordinates of the beginning vertex.</param>
    /// <param name="toID">Coordinates of the end vertex.</param>
    /// <returns>A tuple <from vertex, to vertex, edge weight> if such an edge exists, else null.</returns>
    public Tuple<Vertex<T>, Vertex<T>, float> GetEdge(T fromID, T toID)
    {
        if (HasVertex(fromID) && HasVertex(toID))
        {
            var fromV = Verticies[fromID];
            var toV = Verticies[toID];
            if (fromV.adjacent.ContainsKey(toID))
            {
                return new Tuple<Vertex<T>, Vertex<T>, float>(fromV, toV, fromV.adjacent[toID]);
            }
        }

        return null;
    }

    /// <summary>
    /// Sets the new enumeration method and root.
    /// </summary>
    /// <param name="enumerationMethod">New method of enumeration.</param>
    /// <param name="root">New root node.</param>
    /// <returns>A tuple containing the orignal enumeration method and root.</returns>
    public Tuple<EnumerationMethod, Vertex<T>> SetEnumeration(EnumerationMethod enumerationMethod, Vertex<T> root)
    {
        Tuple<EnumerationMethod, Vertex<T>> og = new(this.EnumMethod, this.Root);

        this.EnumMethod = enumerationMethod;
        this.Root = root;

        return og;
    }

    /// <summary>
    /// Using the <paramref name="enumerationMethod"/> and <paramref name="root"/> vertex,
    /// do an iteration of the graph.
    /// </summary>
    /// <returns>An IEnumerator that is a vertex.</returns>
    public IEnumerator<Vertex<T>> GetEnumerator()
    {
        if (EnumMethod == EnumerationMethod.BreathFirst)
            return BFS();
        else
            return DFS();
    }

    /// <summary>
    /// Using the <paramref name="enumerationMethod"/> and <paramref name="root"/> vertex,
    /// do an iteration of the graph.
    /// </summary>
    /// <returns>An IEnumerator that is a vertex.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    /// <summary>
    /// Breath first traversal, using root. See https://en.wikipedia.org/wiki/Breadth-first_search.
    /// </summary>
    /// <returns>An IEnumerator that is a vertex.</returns>
    private IEnumerator<Vertex<T>> BFS()
    {
        // Breadth first traveral
        Queue<Vertex<T>> q = new Queue<Vertex<T>>();
        q.Enqueue(Root);

        while (q.Count > 0)
        {
            Vertex<T> currV = q.Dequeue();

            if (currV.GetVisited(VERTEX_VISIT_ID)) continue;
            currV.SetVisited(VERTEX_VISIT_ID, true);

            yield return currV;

            foreach (var adjP in currV.adjacent)
            {
                Vertex<T> adjV = Verticies[adjP.Key];

                q.Enqueue(adjV);
            }
        }

        ResetVerticies(VERTEX_VISIT_ID);
    }

    /// <summary>
    /// Depth first traversal, using root. See https://en.wikipedia.org/wiki/Depth-first_search.
    /// </summary>
    /// <returns>An IEnumerator that is a vertex.</returns>
    public IEnumerator<Vertex<T>> DFS()
    {
        // Breadth first traveral
        Stack<Vertex<T>> q = new Stack<Vertex<T>>();
        q.Push(Root);

        while (q.Count > 0)
        {
            Vertex<T> currV = q.Pop();

            if (currV.GetVisited(VERTEX_VISIT_ID)) continue;
            currV.SetVisited(VERTEX_VISIT_ID, true);

            yield return currV;

            foreach (var adjP in currV.adjacent)
            {
                Vertex<T> adjV = Verticies[adjP.Key];

                q.Push(adjV);
            }
        }

        ResetVerticies(VERTEX_VISIT_ID);
    }

    /// <summary>
    /// Performs an A* search of the graph. Assumes graph is fully connected.
    /// </summary>
    /// <param name="startID">What coordinate to start at?</param>
    /// <param name="endID">What coordinate to end at?</param>
    /// <returns>A mapping of <Vertex, Vertex>, where the second vertex is
    /// the vertex that preceeds the first vertex, or empty if a path does not exist.</returns>
    /// <exception cref="ArgumentOutOfRangeException">If startID and/or endID don't exists within the graph.</exception>
    public Dictionary<Vertex<T>, Vertex<T>> AStarSearch(T startID, T endID)
    {
        if (!HasVertex(startID))
            throw new ArgumentOutOfRangeException(nameof(startID), startID, "StartID is out of range!");
        else if (!HasVertex(endID))
            throw new ArgumentOutOfRangeException(nameof(endID), endID, "EndID is out of range!");
        else if (Count < 2)
            return new Dictionary<Vertex<T>, Vertex<T>>();

        PriorityQueue<Vertex<T>> unvisited = new PriorityQueue<Vertex<T>>();
        Dictionary<Vertex<T>, Vertex<T>> path = new Dictionary<Vertex<T>, Vertex<T>>();

        foreach (var vPair in Verticies)
        {
            if ((IEquatable<T>)vPair.Key == (IEquatable<T>)startID)
            {
                unvisited.Enqueue(0, vPair.Value);
            }
            else
            {
                unvisited.Enqueue(float.PositiveInfinity, vPair.Value);
            }
        }

        // G scores are the shortest paths from startV.
        Dictionary<T, float> gScores = new Dictionary<T, float>();
        gScores[startID] = 0;

        var endV = Verticies[endID];
        var startV = Verticies[startID];

        while (unvisited.Count > 0)
        {
            var currentV = unvisited.Dequeue();
            var currentID = currentV.id;

            if ((IEquatable<T>)currentID == (IEquatable<T>)endID)
            {
                // Found end
                path.Reverse();
                return path;
            }
            
            foreach (var adjP in currentV.adjacent)
            {
                // Edge tuple
                var edge = GetEdge(currentID, adjP.Key);

                float newScore = gScores[currentID] + edge.Item3;
                float currScore = (gScores.ContainsKey(adjP.Key)) ? gScores[adjP.Key] : float.PositiveInfinity;
                if (newScore < currScore)
                {
                    // Better path
                    Vertex<T> adjV = Verticies[adjP.Key];
                    gScores[adjP.Key] = newScore;
                    unvisited.Update(newScore + adjV.heuristic(), adjV);
                    path[adjV] = currentV;
                }
            }
        }

        return path;
    }
}