﻿using System.Collections;
using UnityEngine;

public static class AngleExtensions
{
    public enum Range
    {
        PlusMinus180,
        Positive,
        Negative
    }

    /// <summary>
    /// Returns the angle as an angle bounded by the specified range.
    /// </summary>
    /// <param name="range">The range.</param>
    /// <returns>The angle as an angle bounded by the specified range.</returns>
    public static float AsAngleType(this float angle, Range range)
    {
        return range switch
        {
            Range.PlusMinus180 => angle.AsPlusMinus180(),
            Range.Positive => angle.AsPositiveDegrees(),
            Range.Negative => angle.AsNegativeDegrees(),
            _ => throw new System.NotImplementedException()
        };
    }

    /// <summary>
    /// Returns the angle as an angle bounded by the specified range and in degrees or radians.
    /// </summary>
    /// <param name="range">The range.</param>
    /// <param name="radiansOrDegrees">Using radians or degrees?</param>
    /// <returns>
    /// The angle as an angle bounded by the specified range and in degrees or radians.</returns>
    public static float AsAngleType(this float angle, Range range, Angle.Units radiansOrDegrees)
    {
        return radiansOrDegrees switch
        {
            Angle.Units.Degrees => angle.AsAngleType(range),
            Angle.Units.Radians => angle.AsAngleType(range) * Mathf.Deg2Rad,
            _ => throw new System.NotImplementedException()
        };
    }

    /// <summary>
    /// Returns the angle (in degrees) from pointA to pointB.
    /// </summary>
    /// <param name="pointA">The starting point.</param>
    /// <param name="pointB">The end point.</param>
    /// <param name="range">Angle range to return.</param>
    /// <returns>The angle (in degrees) from pointA to pointB.</returns>
    public static float GetAngleToPoint(this Vector2 pointA, Vector2 pointB, Range range)
    {
        return GetAngleFromSeparation(pointA - pointB, range);
    }

    /// <summary>
    /// Gets the angle from the separation vector.
    /// </summary>
    /// <param name="separation">The separation vector.</param>
    /// <param name="range">Angle range to return.</param>
    /// <returns>The specified angle.</returns>
    public static float GetAngleFromSeparation(this Vector2 separation, Range range)
    {
        float x = separation.x;
        float y = separation.y;

        if (x < 0)
        {
            // From http://answers.unity.com/answers/554704/view.html
            return (360 - (Mathf.Atan2(y, x) * Mathf.Rad2Deg) * -1).AsAngleType(range);
        }
        else
        {
            return (Mathf.Atan2(y, x) * Mathf.Rad2Deg).AsAngleType(range);
        }
    }
}
