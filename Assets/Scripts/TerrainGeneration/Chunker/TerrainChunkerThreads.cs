using System.Threading;

public class TerrainChunkerThreads : TerrainChunker
{
    private ShapeGeneratorThread shapeGenerator;
    private Thread shapeThread;
    private CancellationTokenSource cts;
    protected override bool CheckShapeDone()
    {
        return shapeThread == null || !shapeThread.IsAlive;
    }

    protected override bool CheckShapeCancelled()
    {
        return shapeGenerator.Cancelled;
    }

    protected override void CreateShapeGenerator()
    {
        cts = new();

        shapeGenerator = new(crawlOffset, gridPosition, heightResolution,
            heightScale, master.octaves, cts.Token);
        ThreadStart shapeStart = new(shapeGenerator.Generate);
        shapeThread = new(shapeStart);
        shapeThread.Start();
    }

    protected override float[,] GetHeights()
    {
        return shapeGenerator.Retrieve();
    }

    protected override void CleanUp()
    {
        
    }

    protected override float[,,] GetAlphamap()
    {
        throw new System.NotImplementedException();
    }
}