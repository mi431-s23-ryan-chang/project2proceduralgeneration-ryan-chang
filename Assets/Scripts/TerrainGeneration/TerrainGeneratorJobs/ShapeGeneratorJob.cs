﻿using System.Collections.Generic;
using System.Linq;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

/// <summary>
/// Over 5x performace w/ burst compiler.
/// </summary>
[BurstCompile]
public struct ShapeGeneratorJob : IJob
{
    #region Generation variables
    private Vector2 crawlOffset;

    [ReadOnly]
    private NativeArray<Octave> octaves;

    private int samples;
    private float heightScale;
    #endregion
    #region Output variables
    private NativeArray<float> heights;
    #endregion
    #region Control variables
    public bool Cancelled { get; private set; }
    #endregion

    public ShapeGeneratorJob(Vector2 initialCrawlPosition, Vector2Int gridOffset,
        int samples, float heightScale, IEnumerable<Octave> octaves)
    {
        this.crawlOffset = initialCrawlPosition + gridOffset;
        this.octaves = new(octaves.ToArray(), Allocator.Persistent);
        this.samples = samples;
        this.heightScale = heightScale;

        heights = new(samples * samples, Allocator.Persistent);

        Cancelled = false;
    }

    #region Job control
    public void Execute()
    {
        Generate();
    }

    public void Cancel(JobHandle handle)
    {
        Cancelled = true;

        handle.Complete();

        if (heights.IsCreated)
            heights.Dispose();
        
        if (octaves.IsCreated)
            octaves.Dispose();
    }

    public float[,] Retrieve()
    {
        if (Cancelled)
            throw new System.InvalidOperationException("This job has been cancelled.");

        octaves.Dispose();

        float[,] arrHeights = new float[samples, samples];
        
        for (int y = 0; y < samples; y++)
        {
            for (int x = 0; x < samples; x++)
            {
                arrHeights[y, x] = heights[y * samples + x];
            }
        }

        heights.Dispose();
        
        return arrHeights;
    }
    #endregion

    public void Generate()
    {
        foreach (var octave in octaves)
        {
            for (int y = 0; y < samples; y++)
            {
                for (int x = 0; x < samples; x++)
                {
                    if (Cancelled)
                    {
                        Debug.Log("Cancelled");
                        return;
                    }

                    float2 crawlPos = new(
                        (y + crawlOffset.y * (samples - 1)) * octave.frequency,
                        (x + crawlOffset.x * (samples - 1)) * octave.frequency
                    );
                    
                    heights[y * samples + x] += octave.Calculate(crawlPos) * heightScale;
                }
            }
        }
    }
}