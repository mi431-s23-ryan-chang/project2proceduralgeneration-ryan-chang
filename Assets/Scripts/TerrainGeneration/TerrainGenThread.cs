﻿using Unity.Mathematics;
using UnityEngine;

/// <summary>
/// Responsible for generating the heights for the terrain.
/// </summary>
public class TerrainGenThread
{
    public TerrainGenerator TerrainGen { get; private set; }
    public Vector2 InitialCrawlPosition { get; private set; }
    public Vector2Int GridPosition { get; private set; }
    public TerrainData TerrainData { get; private set; }

    private float[,] heights;

    private float heightScale = 1;

    public bool Ready { get; private set; }

    public TerrainGenThread(TerrainGenerator terrainGen, TerrainData data, Vector2 initialCrawlPos, Vector2Int gridPos)
    {
        TerrainGen = terrainGen;
        InitialCrawlPosition = initialCrawlPos;
        GridPosition = gridPos;
        heightScale = data.size.y / terrainGen.terrainHeight;
        TerrainData = data;
        data.size = new(data.size.x, terrainGen.terrainHeight, data.size.z);
    }

    /// <summary>
    /// Gets the generated heights.
    /// Throws InvalidOperationException if the heights
    /// aren't built yet.
    /// </summary>
    /// <returns></returns>
    public float[,] GetHeights()
    {
        if (Ready)
            return heights;
        else
            throw new System.InvalidOperationException("Heights not built yet.");
    }

    public void Generate()
    {
        int samples = TerrainGen.samples;
        heights = new float[samples, samples];

        foreach (var octave in TerrainGen.octaves)
        {
            for (int x = 0; x < samples; x++)
            {
                for (int y = 0; y < samples; y++)
                {
                    float2 crawlPos = new(
                        (x + InitialCrawlPosition.x * (samples - 1)) * octave.frequency,
                        (y + InitialCrawlPosition.y * (samples - 1)) * octave.frequency
                    );
                    
                    float addition;

                    switch (octave.type)
                    {
                        default:
                        case NoiseType.MathfPerlin:
                            addition = Mathf.PerlinNoise(crawlPos.x, crawlPos.y);
                            break;
                        case NoiseType.MathematicsPerlin:
                            addition = noise.cnoise(crawlPos) / 2.3f + 0.5f;
                            break;
                        case NoiseType.Simplex:
                            addition = noise.snoise(crawlPos) / 4.6f + 0.5f;
                            break;
                        case NoiseType.Celluar:
                            addition = noise.cellular(crawlPos).x / 2.3f + 0.5f;
                            break;
                    }
                    heights[y, x] += addition * octave.amplitude * heightScale;
                }
            }
        }

        Ready = true;
        TerrainGen.completedGenerators.Add(this);
    }
}