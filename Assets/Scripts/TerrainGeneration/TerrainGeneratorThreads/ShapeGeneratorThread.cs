﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Unity.Mathematics;
using UnityEngine;

public struct ShapeGeneratorThread
{
    #region Generation variables
    private Vector2 crawlOffset;

    private int samples;

    private float heightScale;

    private Octave[] octaves;
    #endregion
    #region Output variables
    private float[] heights;
    #endregion
    #region Control variables
    public bool Finished { get; private set; }
    public bool Cancelled { get; private set; }
    private CancellationToken token;
    #endregion

    public ShapeGeneratorThread(Vector2 initialCrawlPosition, Vector2Int gridOffset,
        int samples, float heightScale, IEnumerable<Octave> octaves,
        CancellationToken token)
    {
        this.crawlOffset = initialCrawlPosition + gridOffset;
        this.octaves = octaves.ToArray();
        this.samples = samples;
        this.heightScale = heightScale;

        heights = new float[samples * samples];

        Finished = false;
        Cancelled = false;

        this.token = token;
        var self = this;
        this.token.Register(() => self.Cancel());
    }

    #region Job control
    public void Execute()
    {
        Generate();
    }

    public void Cancel()
    {
        Cancelled = true;
    }

    public float[,] Retrieve()
    {
        if (Cancelled)
            throw new System.InvalidOperationException("This job has been cancelled.");

        float[,] arrHeights = new float[samples, samples];
        
        for (int y = 0; y < samples; y++)
        {
            for (int x = 0; x < samples; x++)
            {
                arrHeights[y, x] = heights[y * samples + x];
            }
        }
        
        return arrHeights;
    }
    #endregion

    public void Generate()
    {
        foreach (var octave in octaves)
        {
            for (int y = 0; y < samples; y++)
            {
                for (int x = 0; x < samples; x++)
                {
                    //Debug.Log($"{x}, {y}");
                    if (Cancelled)
                    {
                        Debug.Log("Cancelled");
                        return;
                    }

                    float2 crawlPos = new(
                        (y + crawlOffset.y * (samples - 1)) * octave.frequency,
                        (x + crawlOffset.x * (samples - 1)) * octave.frequency
                    );
                    
                    heights[y * samples + x] += octave.Calculate(crawlPos) * heightScale;
                }
            }
        }

        Finished = true;
    }
}