using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class GraphExtensions
{
    private const string VERTEX_VISIT_ID = "GraphExtInternal";


    /// <summary>
    /// Use <paramref name="renderers"/> to draw a visualization of this graph.
    /// </summary>
    /// <param name="reference">Reference line renderer.</param>
    /// <param name="graph">The graph to trace</param>
    public static void Trace(this Graph<Vector3> graph, LineRenderer reference)
    {
        if (graph.Count < 2) return;

        // Breadth first traveral
        Queue<Vertex<Vector3>> q = new Queue<Vertex<Vector3>>();
        q.Enqueue(graph.Verticies.Values.First());
        GameObject traceContainer = new("TraceContainer");  // Put traces in a container so they are more organized.
        
        while (q.Count > 0)
        {
            Vertex<Vector3> currV = q.Dequeue();

            if (currV.GetVisited(VERTEX_VISIT_ID)) continue;
            currV.SetVisited(VERTEX_VISIT_ID, true);

            foreach (var adjP in currV.adjacent)
            {
                Vertex<Vector3> adjV = graph.Verticies[adjP.Key];

                q.Enqueue(adjV);

                // Now build the renderers
                var rendererGameObj = new GameObject("GridRenderLine");
                rendererGameObj.transform.SetParent(traceContainer.transform);
                rendererGameObj.transform.localPosition = Vector3.zero;
                var newRenderer = reference.CopyComponent(rendererGameObj);
                Vector3[] renderPositions = { currV.id, adjV.id };
                newRenderer.SetPositions(renderPositions);
                newRenderer.enabled = true;
            }
        }

        traceContainer.transform.SetParent(reference.transform);
        traceContainer.transform.localPosition = Vector3.zero;

        graph.ResetVerticies(VERTEX_VISIT_ID);
    }

    /// <summary>
    /// Uses an offset and an x and y coordinates to get the world position of some cell.
    /// </summary>
    /// <param name="grid">Grid to reference.</param>
    /// <param name="offset">Offset, added to the vector created by x and y.</param>
    /// <param name="x">Additional x offset.</param>
    /// <param name="y">Additional y offset.</param>
    /// <returns>The world position of the cell.</returns>
    private static Vector3 CellToWorldOffset(this Grid grid, Vector3Int offset, int x, int y)
    {
        var cellPos = offset;
        cellPos.x += x;
        cellPos.y += y;
        return grid.CellToWorld(cellPos);
    }

    public static Graph<Vector3> MakeRandomGrid(this Grid grid, Vector3Int bottomLeft, float width, float height)
    {
        Graph<Vector3> g = new Graph<Vector3>();
        //bottomLeft += new Vector3Int(1, 1, 0);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                var centerElemPos = grid.CellToWorldOffset(bottomLeft, i, j);
                g.Add(centerElemPos);

                if (i > 0)
                {
                    g.Add(centerElemPos, grid.CellToWorldOffset(bottomLeft, i - 1, j));
                }
                if (j > 0)
                {
                    g.Add(centerElemPos, grid.CellToWorldOffset(bottomLeft, i, j - 1));
                }
                if (i < width - 1)
                {
                    g.Add(centerElemPos, grid.CellToWorldOffset(bottomLeft, i + 1, j));
                }
                if (j < height - 1)
                {
                    g.Add(centerElemPos, grid.CellToWorldOffset(bottomLeft, i, j + 1));
                }
            }
        }

        return g;
    }
}