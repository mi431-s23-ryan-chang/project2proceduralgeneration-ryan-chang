﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

public struct TerrainGenJob : IJob
{
    public Vector2 InitialCrawlPosition { get; private set; }
    private NativeArray<Octave> octaves;
    public int Samples { get; private set; }
    private float[,] heights;
    public bool Ready { get; private set; }

    public TerrainGenJob(Vector2 initialCrawlPos, NativeArray<Octave> octaves,
        int samples)
    {
        heights = null;
        Ready = false;

        InitialCrawlPosition = initialCrawlPos;
        Samples = samples;
        this.octaves = octaves;
    }

    public float[,] GetHeights()
    {
        if (Ready)
            return heights;
        else
            throw new System.InvalidOperationException("Heights not built yet.");
    }

    public void Generate()
    {
        heights = new float[Samples, Samples];

        foreach (var octave in octaves)
        {
            for (int x = 0; x < Samples; x++)
            {
                for (int y = 0; y < Samples; y++)
                {
                    float2 crawlPos = new((x + InitialCrawlPosition.x * (Samples - 1)) * octave.frequency,
                        (y + InitialCrawlPosition.y * (Samples - 1)) * octave.frequency);
                    
                    float addition;

                    switch (octave.type)
                    {
                        default:
                        case NoiseType.MathfPerlin:
                            addition = Mathf.PerlinNoise(crawlPos.x, crawlPos.y);
                            break;
                        case NoiseType.MathematicsPerlin:
                            addition = noise.cnoise(crawlPos) / 2.3f + 0.5f;
                            break;
                        case NoiseType.Simplex:
                            addition = noise.snoise(crawlPos) / 4.6f + 0.5f;
                            break;
                        case NoiseType.Celluar:
                            addition = noise.cellular(crawlPos).x / 2.3f + 0.5f;
                            break;
                    }
                    heights[y, x] += addition * octave.amplitude;
                }
            }
        }

        Ready = true;
    }

    public void Execute()
    {
        Generate();
    }
}