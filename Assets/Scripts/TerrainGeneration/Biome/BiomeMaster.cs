﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// Master class for biome generation.
/// </summary>
public class BiomeMaster : MonoBehaviour
{
    #region Variables
    [Header("Biome Parameters")]
    [InfoBox("Contains the parameters required for each biome.")]
    [OnValueChanged(nameof(UpdateDataTemplate))]
    public List<BiomeParameter> parameters;

    [Header("Biome Data")]
    [InfoBox("Contains the actual biomes. Each biome targets one or more biome " +
        "parameters. Every point on the terrain will be assigned a biome, " +
        "depending on how close the calculated " +
        "parameters are to the parameter target of that biome.")]
    public List<BiomeData> datas;

    #region Debug
    [Header("Debug")]
    public int debugSize = 500;

    public float debugFrequency = 0.1f;
    #endregion
    #endregion

    #region Buttons
    [Button("Debug Biome Parameters")]
    private void DebugParameters()
    {
        for (int i = -debugSize; i < debugSize; i++)
        {
            for (int j = -debugSize; j < debugSize; j++)
            {
                float x = i * debugFrequency;
                float y = j * debugFrequency;
                float mu = 0;

                foreach (var param in parameters)
                {
                    param.DrawDebug(x + mu, y + mu);
                    mu += debugFrequency / 10;
                }
            }
        }
    }

    [Button("Reseed Biome Parameters")]
    private void ReseedParameters()
    {
        foreach (var param in parameters)
        {
            param.ReseedOctaves();
        }
    }

    [Button("Reseed Biome Data")]
    private void ReseedData()
    {
        foreach (var data in datas)
        {
            data.ReseedOctaves();
        }
    }

    [Button("Reseed All")]
    private void ReseedAll()
    {
        ReseedData();
        ReseedParameters();
    }

    [Button("Reset Biome Data")]
    private void ResetData()
    {
        foreach (var d in datas)
        {
            d.ResetParamTargets(parameters);
        }
    }
    #endregion

    #region Biome Data Generation
    private void UpdateDataTemplate()
    {
        foreach (var param in parameters)
        {
            
        }
    }
    #endregion
}