// using System.Collections.Generic;
// using UnityEditor;
// using UnityEngine;

// [CustomPropertyDrawer(typeof(ParameterTargets))]
// public class ParameterTargetDrawerUIE : PropertyDrawer
// {
//     public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//     {
//         // Using BeginProperty / EndProperty on the parent property means that
//         // prefab override logic works on the entire property.
//         EditorGUI.BeginProperty(position, label, property);

//         // ParameterTargets pt = property.GetObjectFromSerialized<ParameterTargets>();

//         // Heights
//         float x = position.x, y = position.y, w = position.width,
//             half_w = w / 2.5f, left_cen = x + w - half_w,
//             lh = EditorGUIUtility.singleLineHeight;

//         // Title
//         GUIStyle bold = new()
//         {
//             fontStyle = FontStyle.Bold,
//         };
//         EditorGUI.LabelField(new(x, y, w, lh), label, bold);
//         y += lh;

//         // List portion
//         var parameters = property.FindPropertyRelative(nameof(ParameterTargets.parameters));
//         var curves = property.FindPropertyRelative(nameof(ParameterTargets.curves));
//         ParameterTargets pt = property.GetObjectFromSerialized<ParameterTargets>();

//         for (int i = 0; i < curves.arraySize; i++)
//         {
//             // Label for curve
//             GUIContent curveLbl = new();
//             EditorGUI.LabelField(new(x, y, w, lh), label);

//             y += lh;
//         }

//         EditorGUI.EndProperty();
//     }

//     public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//     {
//         var paramList = property.FindPropertyRelative(nameof(ParameterTargets.parameters));
//         float lh = EditorGUIUtility.singleLineHeight;
//         float length = paramList.arraySize * lh + lh;
//         return length;
//     }
// }