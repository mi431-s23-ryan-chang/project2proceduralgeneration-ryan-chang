using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ParameterTargets
{
    // [HideInInspector]
    public List<BiomeParameter> parameters;

    // [HideInInspector]
    public List<AnimationCurve> curves;

    public ParameterTargets(List<BiomeParameter> parameters)
    {
        this.parameters = parameters;
        this.curves = new();

        foreach (var p in parameters)
        {
            this.curves.Add(new());
        }

        Debug.Log("yeet");
    }
}